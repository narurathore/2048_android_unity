﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTracker : MonoBehaviour
{
    private int score = 0;

    public static ScoreTracker instance;
    public Text scoreText;
    public Text highScoreText;

    public int Score {
        get
        {
            return score;
        }
        set
        {
            score = value;
            scoreText.text = score.ToString();
            PlayerPrefs.SetInt("score", score);
            if (PlayerPrefs.GetInt("high_score") < score) {
                PlayerPrefs.SetInt("high_score", score);
                highScoreText.text = score.ToString();
            }
        }
    }

    void Awake()
    {
        //PlayerPrefs.DeleteAll();
        instance = this;
        if (!PlayerPrefs.HasKey("high_score"))
        {
            PlayerPrefs.SetInt("high_score", 0);
        }
        if (PlayerPrefs.HasKey("score"))
        {
            score = PlayerPrefs.GetInt("score");
        }
        scoreText.text = score.ToString();
        highScoreText.text = PlayerPrefs.GetInt("high_score").ToString();
    }
}
