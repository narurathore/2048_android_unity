﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tile : MonoBehaviour
{
    public bool mergedInThisTurn = false;
    public int indexRow;
    public int indexCol;

    private Text tileText;
    private Image tileImage;
    private int number;
    private Animator animator;

    public int tileNumber
    {
        get
        {
            return number;
        }
        set
		{
            number = value;
            if (number == 0)
            {
                SetEmpty();
            } else
            {
                SetVisible();
                ApplyStyle(number);
            }
            
		}
    }

    void Awake()
    {
        animator = GetComponent<Animator>();
        tileText = GetComponentInChildren<Text>();
        tileImage = transform.Find("NumberedCell").GetComponent<Image>();
    }

    public void PlayMergedAnimation()
    {
        animator.SetTrigger("Merge");
    }

    public void PlayAppearAnimation()
    {
        animator.SetTrigger("Appear");
    }

    void ApplyStyleFromHolder(int index)
    {
        tileText.text = TileStyleHolder.instance.tileStyles[index].number.ToString();
        tileText.color = TileStyleHolder.instance.tileStyles[index].textColor;
        tileImage.color = TileStyleHolder.instance.tileStyles[index].tileColor;
    }

    void ApplyStyle(int num)
    {
        switch (num)
        {
            case 2:
                ApplyStyleFromHolder(0);
                break;
            case 4:
                ApplyStyleFromHolder(1);
                break;
            case 8:
                ApplyStyleFromHolder(2);
                break;
            case 16:
                ApplyStyleFromHolder(3);
                break;
            case 32:
                ApplyStyleFromHolder(4);
                break;
            case 64:
                ApplyStyleFromHolder(5);
                break;
            case 128:
                ApplyStyleFromHolder(6);
                break;
            case 256:
                ApplyStyleFromHolder(7);
                break;
            case 512:
                ApplyStyleFromHolder(8);
                break;
            case 1024:
                ApplyStyleFromHolder(9);
                break;
            case 2048:
                ApplyStyleFromHolder(10);
                break;
            case 4096:
                ApplyStyleFromHolder(11);
                break;
            case 8192:
                ApplyStyleFromHolder(12);
                break;
            case 16384:
                ApplyStyleFromHolder(13);
                break;
            case 32768:
                ApplyStyleFromHolder(14);
                break;
            case 65536:
                ApplyStyleFromHolder(15);
                break;
            default:
                Debug.LogError("Check the number passed to apply style function");
                break;
        }
    }

    private void SetVisible()
    {
        tileText.enabled = true;
        tileImage.enabled = true;
    }

    private void SetEmpty()
    {
        tileText.enabled = false;
        tileImage.enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
