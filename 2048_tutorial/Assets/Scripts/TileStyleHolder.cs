﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TileStyle {
    public int number;
    public Color32 tileColor;
    public Color32 textColor;
}

public class TileStyleHolder : MonoBehaviour
{

    //Singleton
    public static TileStyleHolder instance;

    public TileStyle[] tileStyles;

    void Awake() {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
